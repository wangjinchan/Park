# Park
#本系统将实现超声波等传感器采集到的信息通过WIFI模块上传到手机端上位机，
#手机APP则可通过发送指令给Wifi模块对出入口门闸进行开关控制，进而实现对地下停车场进行实时监控以及远程控制。
# 本代码是文章 https://wangjinchan.blog.csdn.net/article/details/112346938 的源码

![demo](https://images.gitee.com/uploads/images/2021/0413/112537_285a0431_5201073.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0413/112556_ed9d03bd_5201073.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0413/112610_41cc135b_5201073.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0413/112620_fe76c405_5201073.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0413/112633_d0189912_5201073.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0413/112642_ed58cb05_5201073.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0413/112655_f417ad6c_5201073.png "屏幕截图.png")
## 测试方法
## 在电脑利用网络调试助手软件，开启一个端口号为8080的tcp服务端，注意安卓APP的ip地址要改成电脑本机的ip地址，不然连接不成功的。
## 连接成功之后，就可以尝试电脑发送我上述所讲的命令，也可以尝试用APP对出入闸开关按钮进行点击。